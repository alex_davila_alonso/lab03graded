package linearalgebra;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * The Vector3dTests class is a class that will test each of my methods from
 * the Vector3d class
 * 
 * @author Alex Davila Alonso 2132425
 * @version 09/09/2022
 */
public class Vector3dTests {

    /**
     * This method will test that the values returned from the get methods are
     * correct
     */
    @Test
    public void testGetMethods() {
        Vector3d testVector = new Vector3d(4.0, 5.0, 9.0);
        assertEquals(4.0, testVector.getX(), 0);
        assertEquals(5.0, testVector.getY(), 0);
        assertEquals(9.0, testVector.getZ(), 0);

    }

    /**
     * This method will test that the values we get calculating magnitude is alright
     */
    @Test
    public void testMagnitude() {
        Vector3d testVector = new Vector3d(4, 5, 9);
        assertEquals(11, testVector.magnitude(), 0.05);
    }

    /**
     * this methods tests that the method that calculates the dotproduct gives
     * correct result
     */
    @Test
    public void testDotProduct() {
        Vector3d testVector = new Vector3d(4, 5, 9);
        Vector3d testVector2 = new Vector3d(2, 3, 5);
        assertEquals(68, testVector.dotProduct(testVector2), 0);
    }

    /**
     * This methods tests the add method to make sure the sum of the vectors give
     * correct results
     */
    @Test
    public void testAdd() {
        Vector3d testVector = new Vector3d(4, 5, 9);
        Vector3d testVector2 = new Vector3d(2, 3, 5);
        Vector3d expectedVector = new Vector3d(6, 8, 14);
        Vector3d theVector = testVector.add(testVector2);
        assertEquals(expectedVector.getX(), theVector.getX(), 0);
        assertEquals(expectedVector.getY(), theVector.getY(), 0);
        assertEquals(expectedVector.getZ(), theVector.getZ(), 0);
    }

}
