
package linearalgebra;

/**
 * Vector3d is a class used to do operations on some vectors
 * like finding the magnitude, the dot product and adding vectors
 * 
 * 
 * 
 * @author ALEX DAVILA ALONSO 2132425
 * @version 09/09/2022
 */
public class Vector3d {
    private double x;
    private double y;
    private double z;

    /**
     * Initializes the Vector3d constructor
     * 
     * @param x
     * @param y
     * @param z
     */
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * get method to return x
     * 
     * @return the value of x
     */
    public double getX() {
        return this.x;
    }

    /**
     * get method to return y
     * 
     * @return the value of y
     */
    public double getY() {
        return this.y;
    }

    /**
     * get method to return z
     * 
     * @return the value of z
     */
    public double getZ() {
        return this.z;
    }

    /**
     * This method finds the magnitude of a vector
     * 
     * @return the total value of the magnitude
     */
    public double magnitude() {
        double magnitude;
        magnitude = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
        return magnitude;
    }

    /**
     * This method find the dot product of two vectors, as it takes a vector
     * as a parameter
     * 
     * @param vector
     * @return the total dot product of the vectors
     */
    public double dotProduct(Vector3d vector) {
        double dotProduct = (this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ());
        return dotProduct;
    }

    /**
     * This method adds two vectors together
     * 
     * @param vector
     * @return the sum of both vectors
     */
    public Vector3d add(Vector3d vector) {
        Vector3d vector2 = new Vector3d(this.x + vector.getX(), (this.y + vector.getY()), (this.z + vector.getZ()));

        return vector2;
    }

}
